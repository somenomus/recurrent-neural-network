'''
a simpleRNN example. first one normal, 2nd one using contrib lib. comment out each other to see different
arcitecture.
'''

import tensorflow as tf
import numpy as np

#input and neuron number
inp = 3
neu = 5
x1 = tf.placeholder(tf.float32, [None, inp])
x2 = tf.placeholder(tf.float32, [None, inp])

wx = tf.get_variable("wx", shape=[inp, neu],dtype=tf.float32, initializer=None, regularizer=None, trainable=True, collections=None)

wy = tf.get_variable("wy", shape=[neu, neu],dtype=tf.float32, initializer=None, regularizer=None, trainable=True, collections=None)

b = tf.get_variable("b", shape=[1, neu],dtype=tf.float32, initializer=None, regularizer=None, trainable=True, collections=None)

y1 = tf.nn.relu(tf.matmul(x1, wx) + b)
y2 = tf.nn.relu(tf.matmul(y1, wy) + tf.matmul(x2, wx) + b)

init_op = tf.global_variables_initializer()

x1_batch = np.array([[0, 2, 3], [2, 8, 9], [5, 3, 8], [3, 2, 9]])
x2_batch = np.array([[5, 6, 8], [1, 0, 0], [8, 2, 0], [2, 3, 6]])

#this net seems feed forword net, but both layers share same weight and bias.And we also will feed input each layer
#and get output from each layer

with tf.Session() as sess:
    init_op.run()
    y1_out, y2_out = sess.run([y1, y2], feed_dict = {x1: x1_batch, x2: x2_batch})


print(y1_out)   #output at t = 0
print(y2_out)   #output at t = 1



'''
n_inputs = 3
n_neurons = 5
X1 = tf.placeholder(tf.float32, [None, n_inputs])
X2 = tf.placeholder(tf.float32, [None, n_inputs])

basic_cell =  tf.nn.rnn_cell.BasicRNNCell(num_units=n_neurons)
output_seqs, states = tf.contrib.rnn.static_rnn(basic_cell, [X1, X2], dtype=tf.float32)

Y1, Y2 = output_seqs

init_op = tf.global_variables_initializer()

# Mini-batch: instance 0,instance 1,instance 2,instance 3
X1_batch = np.array([[0, 2, 3], [2, 8, 9], [5, 3, 8], [3, 2, 9]]) # t = 0
X2_batch = np.array([[5, 6, 8], [1, 0, 0], [8, 2, 0], [2, 3, 6]]) # t = 1

with tf.Session() as sess:
	init_op.run()
	Y1_val, Y2_val = sess.run([Y1, Y2], feed_dict={X1: X1_batch, X2: X2_batch})

print(Y1_val) # output at t = 0
print(Y2_val) # output at t = 1
'''


'''
n_inputs = 3
n_neurons = 5
n_steps = 2

X = tf.placeholder(tf.float32, [None, n_steps, n_inputs])
seq_length = tf.placeholder(tf.int32, [None])

basic_cell = tf.nn.rnn_cell.BasicRNNCell(num_units=n_neurons)
output_seqs, states = tf.nn.dynamic_rnn(basic_cell, X, dtype=tf.float32)

# Mini-batch: instance 0,instance 1,instance 2,instance 3
X_batch = np.array([
                   [[0, 2, 3], [2, 8, 9]], # instance 0
		   [[5, 6, 8], [0, 0, 0]], # instance 1 (padded with a zero vector)
                   [[6, 7, 8], [6, 5, 4]], # instance 2
                   [[8, 2, 0], [2, 3, 6]], # instance 3
                  ])

seq_length_batch = np.array([3, 4, 3, 5])

init_op = tf.global_variables_initializer()

with tf.Session() as sess:
	init_op.run()
	outputs_val, states_val = sess.run([output_seqs, states], feed_dict={X: X_batch, seq_length: seq_length_batch})

print(outputs_val)
print(states_val)
'''
